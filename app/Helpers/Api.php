<?php
use Illuminate\Http\Request;

if (!function_exists('getApiGuard')) {

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    function getApiGuard() {
        return Auth::guard('api');
    }

}


if (!function_exists('getApiUser')) {

    /**
     * Get the guard to be used during authentication.
     *
     * 
     */
    function getApiUser() {
        return getApiGuard()->user();
//      return auth('api')->user();
    }

}