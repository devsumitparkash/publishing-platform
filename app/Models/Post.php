<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;

class Post extends Model {

  use HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title', 'description', 'website_id', 'user_id'
  ];

  public function website(): belongsTo {
    return $this->belongsTo(Website::class);
  }

  public function user(): belongsTo {
    return $this->belongsTo(User::class);
  }

}
