<?php

namespace App\Traits;

use App\Models\Token;
use App\Http\Resources\User\User as UserResource;

trait RespondWithToken {

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithToken($user, $status = 200) {
        $request = request();
        
        if(!$user->api_token){
            $user->api_token = Token::generate();
            $user->save();
        }
        
        $request->merge($this->getTokenContract($user->api_token));
        
        return (new UserResource($user))
                ->response()
                ->setStatusCode($status);
        
    }

    protected function getTokenContract($token) {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
    }

}
