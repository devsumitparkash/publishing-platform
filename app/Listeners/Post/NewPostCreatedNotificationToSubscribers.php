<?php

namespace App\Listeners\Post;

use App\Mail\Post\NewPostCreated;
use App\Events\Post\PostCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class NewPostCreatedNotificationToSubscribers implements ShouldQueue {

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  object  $event
   * @return void
   */
  public function handle(PostCreated $event) {
    $post = $event->post;

    $website = $post->website;

    $subscribers = $website->subscribers;

    if ($subscribers->count()) {
      foreach ($subscribers as $subscriber) {
        Mail::to($subscriber->email)->send(new NewPostCreated($post));
      }
    }
  }

}
