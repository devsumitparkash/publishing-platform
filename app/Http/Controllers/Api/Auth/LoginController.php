<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Traits\RespondWithToken;

class LoginController extends Controller {

    use AuthenticatesUsers,
        RespondWithToken
    ;

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
    */
    protected function authenticated(Request $request, $user)
    {
        return $this->respondWithToken($user);
    }
}
