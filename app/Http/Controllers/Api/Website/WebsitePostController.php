<?php

namespace App\Http\Controllers\Api\Website;

use App\Events\Post\PostCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Post\StorePostRequest;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use App\Models\Website;
use Illuminate\Http\Request;

class WebsitePostController extends Controller {

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StorePostRequest $request, Website $website) {
    $post = $website->posts()->create(
            array_merge(
                    $request->all(),
                    [
                        'user_id' => request()->user()->id,
                    ]
    ));

    event(new PostCreated($post));

    return new PostResource(
            $post
    );
  }

}
