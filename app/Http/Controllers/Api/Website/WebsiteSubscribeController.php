<?php

namespace App\Http\Controllers\Api\Website;

use App\Models\Website;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class WebsiteSubscribeController extends Controller
{
  public function subscribe(Website $website){
    $user = request()->user();

    //$website->subscribers()->attach($user);
    $website->subscribers()->syncWithoutDetaching($user);

    return response(null, Response::HTTP_OK);
  }
  
  
  public function unsubscribe(Website $website){
    $user = request()->user();

    $website->subscribers()->detach($user);   
    return response(null, Response::HTTP_OK);
  }

}
