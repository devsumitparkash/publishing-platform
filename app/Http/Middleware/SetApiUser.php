<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SetApiUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!Auth::user()) {
            $user = getApiUser();
        
        if($user){
            Auth::login($user);
        }
        }
        
        
        
        return $next($request);
    }
}
