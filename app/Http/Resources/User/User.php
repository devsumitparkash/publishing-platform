<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource {
    
    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array {

        return [
            $this->mergeWhen($request->filled('access_token'), [
            'access_token' => $request->access_token,
            'token_type' => $request->token_type,
            ]),
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }

}
