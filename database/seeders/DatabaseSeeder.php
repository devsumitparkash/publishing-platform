<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\User;
use App\Models\Website;

class DatabaseSeeder extends Seeder {

  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {
    User::factory(20)->create();

    $users = User::all();

    Website::factory(20)
            ->create()
            ->each(function ($website) use ($users) {
              Post::factory(5)
              ->create([
                  'user_id' => rand(1, 10),
                  'website_id' => $website->id
              ]);

              $website->subscribers()->attach(
                      $users->random(rand(1, 3))->pluck('id')->toArray()
              );
            });
  }

}
