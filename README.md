## Installation

Use PHP Version 7.4

git clone git@bitbucket.org:devsumitparkash/publishing-platform.git

cd publishing-platform

cp .env.example .env

composer install 

php artisan migrate

php artisan db:seed


