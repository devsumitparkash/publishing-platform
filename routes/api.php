<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\RegisterrController;
use App\Http\Controllers\Api\Website\WebsiteSubscribeController;
use App\Http\Controllers\Api\Website\WebsitePostController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});

Route::group(['as' => 'api.', 'middleware' => ['set.api.user',]], function () {
  Route::group([
          ], function () {

            Route::post('register', [RegisterController::class, 'register']);
            Route::post('login', [LoginController::class, 'login']);
          });

  Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'website', 'as' => 'website.'], function () {
      Route::post('/{website}/subscribe', [WebsiteSubscribeController::class, 'subscribe']);
      Route::post('/{website}/unsubscribe', [WebsiteSubscribeController::class, 'unsubscribe']);

      Route::post('/{website}/post', [WebsitePostController::class, 'store']);
    });
  });
});

